<?php
dpm($form);
?>

<?php if (empty($cart)): ?>
  <p><?php print t('Your shopping cart is empty.'); ?></p>
<?php else: ?>
	<table class="basic-cart-cart-content-table">
		<tr class="header-row">
			<th class="item-name-col">Наименование</th>
			<th class="item-count-col">Количество</th>
			<th class="item-price-col">Цена, с НДС</th>
			<th class="item-total-col">Сумма</th>
			<th class="item-actions-col"></th>
		</tr>
		<?php foreach(element_children($form['cartcontents']) as $nid): ?>
		<?php 
			$node = $cart[$nid];
			$remove_button = drupal_render($form['cartcontents'][$nid]['remove']);
		?>
		<tr id="cart-item-<?php print $node->nid?>-row" class="cart-item-row cart-item-<?php print $node->nid?>-row">
			<td class="item-name-col">
				<?php print l($node->title, 'node/' . $node->nid)?>
			</td>
			<td class="item-count-col">
				<?php print drupal_render($form['cartcontents'][$nid])?>
			</td>
			<td class="item-price-col">
				<?php print $form['cartcontents'][$nid]['#unit_formated_price'];?> c НДС
			</td>
			<td class="item-total-col">
				<?php print $form['cartcontents'][$nid]['#unit_formated_total_price']?> c НДС
			</td>
			<td class="item-actions-col">
				<?php print $remove_button;?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	
	<?php print drupal_render($form['buttons']);?>
		
	<?php print drupal_render_children($form);?>
	
<?php endif; ?>
