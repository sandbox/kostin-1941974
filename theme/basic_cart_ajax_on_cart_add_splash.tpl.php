<?php
?>
<div class="basic-cart-on-cart-add-splash" style="display:none">
	<div class="splash-inner-wrapper">
		<div class="splash-close-button">x</div>
		<div class="basic-cart-splash-message">
			Вы положили товар в корзину.<br>
			Хотите сразу перейти к <?php print l('оформлению заказа', 'checkout')?>?
		</div>
	</div>
</div>